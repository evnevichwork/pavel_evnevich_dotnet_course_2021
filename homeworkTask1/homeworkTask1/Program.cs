﻿using System;

namespace homeworkTask1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите первое число = ");
            int num1 = int.Parse(Console.ReadLine());

            Console.Write("Введите второе число = ");
            int num2 = int.Parse(Console.ReadLine());

            Swap(ref num1, ref num2);
        }
        public static void Swap(ref int num1, ref int num2)
        {
            num1 = num1 + num2;
            num2 = num1 - num2;
            num1 = num1 - num2;

            Console.Write($"Первое число = {num1}, второе число = {num2} ");
        }
    }
}
